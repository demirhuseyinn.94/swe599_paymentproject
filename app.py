from operator import le
from flask import Flask, render_template, request, flash
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
from sqlalchemy import ForeignKey
from sqlalchemy import select
from sqlalchemy import engine, create_engine
import json
from sqlalchemy.sql.expression import false, update
from flask_swagger_ui import get_swaggerui_blueprint
from flask_cors import CORS
import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@localhost/bankrepo'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.secret_key = 'secret string'
engine = create_engine("postgresql://postgres:postgres@localhost/bankrepo")
db = SQLAlchemy(app)


class account(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    accountName = db.Column(db.String(80), nullable=False)
    totalAmount = db.Column(db.Float, nullable=False, default=0.0)
    isBlocked = db.Column(db.Boolean, nullable=False)
    userId = db.Column(db.Integer, ForeignKey("user.id"), nullable=False)
    userModel = relationship("user")

    def __init__(self, accountName, userId, isBlocked):
        self.accountName = accountName
        self.userId = userId
        self.isBlocked = isBlocked


class user(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    userName = db.Column(db.String(80), nullable=False)
    userLastName = db.Column(db.String(120), nullable=False)
    userPhoneNumber = db.Column(db.String(120), nullable=False)
    userMail = db.Column(db.String(120), nullable=False)
    account = relationship("account")

    def __init__(self, userName, userLastName, userPhoneNumber, userMail):
        self.userName = userName
        self.userLastName = userLastName
        self.userPhoneNumber = userPhoneNumber
        self.userMail = userMail


class transactions(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    userId = db.Column(db.Integer, nullable=False)
    accountId = db.Column(db.Integer, nullable=False)
    transactionDate = db.Column(
        db.DateTime, default=datetime.datetime.now(), nullable=False)
    isSuccessfull = db.Column(db.Boolean, nullable=False)
    transactionType = db.Column(db.String(120))

    def __init__(self, userId, accountId, isSuccessfull,transactionType):
        self.userId = userId
        self.accountId = accountId
        self.isSuccessfull = isSuccessfull
        self.transactionType = transactionType


@app.route('/')
def home():
    f = open('static/swagger.json')
    data = json.load(f)
    return data


@app.route("/getUsers", methods=['GET'])
def getUser():
    stmt = select(user)
    print(stmt)
    connection = engine.connect()
    results = connection.execute(stmt).fetchall()
    print(results)

    return "userya"


@app.route("/addUser", methods=['POST'])
def personadd():
    record = json.loads(request.data)

    userName = record['userName']
    userLastName = record['userLastName']
    userPhoneNumber = record['userPhoneNumber']
    userMail = record['userMail']
    new_user = user(userName=userName, userLastName=userLastName,
                    userPhoneNumber=userPhoneNumber, userMail=userMail)
    db.session.add(new_user)
    db.session.commit()
    return record


@app.route("/addAccount", methods=['POST'])
def addAcount():
    record = json.loads(request.data)

    accountName = record['accountName']
    totalAmount = 0.0
    isBlocked = bool(False)
    userId = record['userId']
    newAccount = account(accountName=accountName,
                         userId=userId, isBlocked=isBlocked)
    db.session.add(newAccount)
    db.session.commit()
    return record


@app.route("/deposit/<userid>", methods=['POST'])
def deposit(userid):
    try:
        record = json.loads(request.data)
        accountId = record['accountid']
        newAmount = record['newAmount']
        userQuery = select(user).where(user.id == userid)
        connection = engine.connect()
        userData = connection.execute(userQuery).first()
        dataCount = len(userData)
        if dataCount > 0:
            accountQuery = select(account).where(account.userId == userid).where(
                account.isBlocked == False).where(account.id == accountId)
            connection = engine.connect()
            accountData = connection.execute(accountQuery).first()
            accountCount = len(accountData)
            if accountCount >= 1:
                updateDepositQuery = update(account).where(account.userId == userid).where(
                    account.isBlocked == False).values(totalAmount=account.totalAmount+newAmount)
                connection.execute(updateDepositQuery)
                db.session.commit()
                newTransaction = transactions(
                    accountId=accountId, userId=userid, isSuccessfull=True,transactionType="deposit")
                db.session.add(newTransaction)
                db.session.commit()
                return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
            else:
                newTransaction = transactions(
                    accountId=accountId, userId=userid, isSuccessfull=False,transactionType="deposit")
                db.session.add(newTransaction)
                db.session.commit()
                return json.dumps({'success': True}), 500, {'ContentType': 'application/json'}
        else:
            newTransaction = transactions(
                accountId=accountId, userId=userid, isSuccessfull=False,transactionType="deposit")
            db.session.add(newTransaction)
            db.session.commit()
            return json.dumps({'success': True}), 500, {'ContentType': 'application/json'}
    except Exception as transactionException:
        newTransaction = transactions(
            accountId=accountId, userId=userid, isSuccessfull=False,transactionType="deposit")
        db.session.add(newTransaction)
        db.session.commit()
        return json.dumps({'success': False}), 500, {'ContentType': 'application/json'}


@app.route("/withdraw/<userid>", methods=['POST'])
def withdraw(userid):
    try:
        record = json.loads(request.data)
        accountId = record['accountid']
        newAmount = record['newAmount']
        userQuery = select(user).where(user.id == userid)
        connection = engine.connect()
        userData = connection.execute(userQuery).first()
        dataCount = len(userData)
        if dataCount > 0:
            accountQuery = select(account).where(account.userId == userid).where(
                account.isBlocked == False).where(account.id == accountId)
            connection = engine.connect()
            accountData = connection.execute(accountQuery).first()
            accountCount = len(accountData)
            if accountCount >= 1:
                updateDepositQuery = update(account).where(account.userId == userid).where(
                    account.isBlocked == False).values(totalAmount=account.totalAmount-newAmount)
                connection.execute(updateDepositQuery)
                db.session.commit()
                newTransaction = transactions(
                    accountId=accountId, userId=userid, isSuccessfull=True,transactionType="deposit")
                db.session.add(newTransaction)
                db.session.commit()
                return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
            else:
                newTransaction = transactions(
                    accountId=accountId, userId=userid, isSuccessfull=False,transactionType="deposit")
                db.session.add(newTransaction)
                db.session.commit()
                return json.dumps({'success': True}), 500, {'ContentType': 'application/json'}
        else:
            newTransaction = transactions(
                accountId=accountId, userId=userid, isSuccessfull=False,transactionType="deposit")
            db.session.add(newTransaction)
            db.session.commit()
            return json.dumps({'success': True}), 500, {'ContentType': 'application/json'}
    except Exception as transactionException:
        newTransaction = transactions(
            accountId=accountId, userId=userid, isSuccessfull=False,transactionType="deposit")
        db.session.add(newTransaction)
        db.session.commit()
        return json.dumps({'success': False}), 500, {'ContentType': 'application/json'}


@app.route("/getTransactionsByUserId/<userid>", methods=['GET'])
def getTransactionsByUserId(userid):

    transactionQuery = select(transactions).where(
        transactions.userId == userid)
    connection = engine.connect()
    results = connection.execute(transactionQuery).fetchall()
    resultList=[]
    for result in results:
        print(result)
        transactionRecord={
        "userId" : result[1],
        "accountId" : result[2],
        "transactionType" : result[5],
        "date" : str(result[3]),
        "isSuc" : result[4]
        }
        resultList.append(transactionRecord)
    jsonString = json.dumps(resultList,indent=4)
    return jsonString


@app.route("/getTransactionsByAccountId/<accountid>", methods=['GET'])
def getTransactionsByAccountId(accountid):

    transactionQuery = select(transactions).where(
        transactions.accountId == accountid)
    connection = engine.connect()
    results = connection.execute(transactionQuery).fetchall()
    resultList=[]
    for result in results:
        print(result)
        transactionRecord={
        "userId" : result[1],
        "accountId" : result[2],
        "transactionType" : result[5],
        "date" : str(result[3]),
        "isSuc" : result[4]
        }
        resultList.append(transactionRecord)
    jsonString = json.dumps(resultList,indent=4)
    return jsonString

if __name__ == '__main__':
    SWAGGER_URL = '/swagger'
    API_URL = 'http://localhost:5000'
    SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
        SWAGGER_URL,
        API_URL,
        config={
            'app_name': "Seans-Python-Flask-REST-Boilerplate"
        }
    )
    CORS(app)
    app.register_blueprint(SWAGGERUI_BLUEPRINT)

    db.create_all()
    app.run(host="0.0.0.0", port=5000)

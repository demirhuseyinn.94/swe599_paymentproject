# swe599_paymentproject

# SWE 599 Payment System Project Repository

# How to run project in local environment for test purposes

1. Clone the project

- HTTPS : git clone https://gitlab.com/demirhuseyinn.94/swe599_paymentproject.git
- SSH   : git clone git@gitlab.com:demirhuseyinn.94/swe599_paymentproject.git


2. Go to project folder

```bash
cd swe599_paymentproject/
```

3. Run docker-compose command in order to build local environment

```bash
docker-compose up -d
```
4. Check if everything works

```bash
docker ps
```

5. Test endpoint
```bash
curl localhost:5000/test
```

# Contributors

Hüseyin DEMİR